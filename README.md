# FrappuccinoJS

Awesome cute components library to be used by awesome people who love frappus and coffee 😊 ❤️

## Quick install

FrappuccinoJS is constantly in development! Try it out now:

### NPM

```sh
npm install frappu
```

**or**

### Yarn

```sh
yarn add frappu
```

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

## TODO

- [] Add Atomic components
- [] Add Molecular components
- [] Add Organisms components
- [] Add Template components
  
