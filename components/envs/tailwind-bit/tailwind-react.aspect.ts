import { Aspect } from '@teambit/harmony';

export const TailwindReactAspect = Aspect.create({
  id: 'pt.frappuccino/envs/tailwind-react',
  defaultConfig: {},
});
